#CSS1

##1. Getting Started:

- HTML defines the webpage structure and meaning of the content to the browser.

- HTML and CSS work closely together.

- Front end = HTML + CSS + Javascript

- CSS: presentation layer, defines styles (e.g., colors)

- Javascript: adds interactivity to the webpage

- Add CSS: inline, internal and external

##2. CSS Core:

- Selectors determine which HTML element to apply styles to.

- Declaration blocks consists of one or more styles rules, enclosed in curly braces {}.

- Class selector:

	+ You define the value
		
	+ Can be used multiple times per page

- ID selector:

	+ You define the value	
		
	+ Can only be used once per page
		
- Color resources:

	+ http://colours.neilorangepeel.com/
		
	+ https://coolors.co/
		
	+ https://randoma11y.com/#/?_k=wdragy
		
- CSS specificity:

	+ Determines which CSS rule take precedence. The selector with the higher specificity will by applied
		
	+ Resources:
				
		+ http://cssspecificity.com/
				
		+ https://specificity.keegan.st/

##3. Typography:

- Typography: the study of the design and use of type for communication

- Typeface: A set of fonts, designed with common characteristics and composed of glyphs

	+ Serif: Have small decorative lines
		
	+ Sans-serif: Have no decorative lines
		
	+ Script: hand-lettered look
		
	+ Decorative: Distinct and ornamental
		
	+ Monospace: Each character uses the same amount of horizontal space
		
- Font: Individual files that are part of a typeface

- Web_Safe fonts: Fonts that are commonly preinstalled on devices

- Using a Font Stack:

	+ Choose similar fonts
			
	+ Use commas to separate each option
			
	+ Use a generic font family as the last option
			
	+ Always declare generic fonts without 
		
- Web fonts:

	+ Internal source
			
		+ Downloaded font files
				
		+ Included in project files
				
		+ Declare and link to font files using @font-face
				
		+ Different browsers support different font format

		+Resources: https://css-tricks.com/snippets/css/using-font-face/ or https://www.fontsquirrel.com/tools/webfont-generator

	+ External source
			
		+ Third-party online service (Typekit, Google Fonts, etc.)

		+ No need to download

		+ Link directly to CSS and font files hosted online

- Font size:

	+ px: Default 16px
		
	+ em: 1em = inherited font-size = default 16px
		
	+ rem: 1rem = 1em = default 16px. Relative only to the root element (HTML)

	+ Relative values are calculated based on the nearest ancestor element

	+ Absolute values are not affected by ancestor elements

##4. Layouts:

- Block vs inline display

	+ Block: 
		
		+ Height = content
				
		+ Width = 100% of container
				
		+ Elements start on a new line
				
		+ Can wrap other block and inline elements
				
	+ Inline:
				
		+ Height and width = content
				
		+ Elements align left in a line
				
		+ Can only nest other inline elements (except <a> tag)
				
- The box model: margin, border, padding, width/height

- The box model fix:
	
	```
	html {
		box-sizing: border-box;
	}

	*, *:before, *:after {
		box-sizing: inherit;
	}
	```

#CSS2
	
##1. CSS Selectors

- Selectors:

	+ All other attribute selectors follow this format:
	
		+ [attr] {/*Select the matched attribute*/}
			
		+ [attr=val] {/*Select the matched attribute and value*/}
	
	+ Descendant Selectors: Descendant selector: Selects any element that's nested inside (ancestor parent/child/...)
	
	+ Child selector: Selects the first child element only - the element that follows right after the parent element (parent > child)
	
	+ Sibling combinators:
			
		+ Adjacent (+): Selects only the following chosen element
			
		+ General (~): Selects all the chosen element that follows
	
	+ Grouping multiple selectors with commas: p1, h2, a, ... {}
	
	+ Combining multiple class value: .button.submit.cancel... {}
	
	+ Pseudo-class Selectors:
			
		+ A keyword, added to a selector with the colon symbol, used to specify a certain state.
			
		+ :first-child: Selects the first child element of the parent
			
		+ :last-child: Selects the last child element of the parent
			
		+ :first-of-type: Selects the first child element of its type
			
		+ :last-of-type: Selects the last child elemnt of its type
			
		+ :nth-child: Selects one or more child elements based on the order within the parent container. 
			
		+ :nth-of-type: Same as above
	
	+ Pseudo-element selector: Selects certain parts of the elements that are not a part of the DOM tree (Creating new fake elements?). E.g.:
:before/:after: Inserting string content

##2. Layouts

-  Horizontal navigation without list:

	+ Using inline-block:
				
		+ HTML renders line feed as a white space
				
		+ Reduce font size to 0 to clear the white space
				
		+ Restore font size for the link
				
	+ Using float
		
- Position:

	+ Used to arrange elements relative to the default page flow or browser viewport. Values: relative, absolute, fixed, static, inherit
			
		+ Relative: Elements stay in natural flow
			
		+ Absolute: Elements removed from natural flow
			
		+ Fixed: Similar to absolute, but always relative to the viewport and stay fixed at one place
			
		+ Static: Default. Elements not positioned at all
			
		+ Inherit: Inherits value from ancestor element
			
	+ Used with a combination of offset properties: top, right, bottom, left
	
-  Float:
		
	+ Variable and flexible content
		
	+ Global or large page structures

- Display:
		
	+ Aligning page components (beware of extra space)
		
	+ Aligning elements that need to be center aligned
		
	+ Doesn't change the natural page flow

- Position:

	+ Positioning elements relative to another element
		
	+ Aligning elements outside of the document flow

	+ Aligning elements to a specific spot
 
 Note: Three of these options cannot be used on the same element
 
- Layers and z-index: Natural stacking order: block < float < inline < position


##3. Tips and Tools

- Reset stylesheet: Stylesheet containing rules that override all the default browser styles to an un-styled baseline

- Normalize: Stylesheet containing rules that aim to create consistent default styles, rather than removing them

- Icon fonts
	
	+ Add imagery without using images

	+ Can be styled with CSS

	+ Font Awesome: http://fontawesome.io/

- Background property

	+ Background image's property: https://www.w3schools.com/cssref/pr_background-image.asp

	+ 

- Alpha transparency and gradients
	
	+ Alpha transparency syntax: rgba(R, G, B, Alpha Transparency (from 0 to 1))

	+ Can use gradient as background: linear-gradient(color, color)
	
	+  Overlaying color on a background image:
		
		+ Use alpha transparency (need 2 containers)

		+ Use gradient: background: linear-gradient(rgba(...), rgba(...)), url(...) ...


##4. Responsive and Mobile

- Responsive website aims to be device-agnostic and optimized for any screen using one website and one code base
(reference: mediaqueri.es -> used to specify how a document is presented on different media)

- Fluid layouts are relative, but the content and components only get wider or narrower

- Responsive layouts change based on sreen size

- Media queries help to create conditions for applying specific CSS styles:

	+ Has two components: media types and media features
	
	+ Syntax:
		
		+ Can be added to the <link>: `<link media="screen and (max-width: 400px)" rel="stylesheet" href="mobile.css">`
		
		+ Using the @media keyword and curly braces {}
		
		+ Use more than one condition and combine min- and max-width to specify a range, specify by and
		
- Media query best practices
	
	+ Try not to use too many media queries

	+ Combine the ones that are close in size

	+ Try not to get fixated on specific screen sizes

	+ Add a breakpoint when the design itself needs it
